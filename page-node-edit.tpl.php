<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php echo get_page_language($language); ?>" xml:lang="<?php echo get_page_language($language); ?>">

<head>
  <title><?php if (isset($head_title )) { echo $head_title; } ?></title>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <?php echo $head; ?>  
  <?php echo $styles ?>
  <?php echo $scripts ?>
  <!--[if IE 6]><link rel="stylesheet" href="<?php echo $base_path . $directory; ?>/style.ie6.css" type="text/css" /><![endif]-->  
  <!--[if IE 7]><link rel="stylesheet" href="<?php echo $base_path . $directory; ?>/style.ie7.css" type="text/css" media="screen" /><![endif]-->
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>

<body>
<div id="idt-page-background-simple-gradient">
</div>
<div id="idt-page-background-glare"><div id="idt-page-background-glare-image"></div>
</div>
<div id="idt-main">
<div class="idt-Sheet">
    <div class="idt-Sheet-tl"></div>
    <div class="idt-Sheet-tr"></div>
    <div class="idt-Sheet-bl"></div>
    <div class="idt-Sheet-br"></div>
    <div class="idt-Sheet-tc"></div>
    <div class="idt-Sheet-bc"></div>
    <div class="idt-Sheet-cl"></div>
    <div class="idt-Sheet-cr"></div>
    <div class="idt-Sheet-cc"></div>
    <div class="idt-Sheet-body">
<div class="idt-Header">
    <div class="idt-Header-png"></div>
<div class="idt-Logo">
    <?php 
        if (!empty($site_name)) { echo '<h1 class="idt-Logo-name"><a href="'.check_url($base_path).'" title = "'.$site_name.'">'.$site_name.'</a></h1>'; }
        if (!empty($site_slogan)) { echo '<div class="idt-Logo-text">'.$site_slogan.'</div>'; }
        if (!empty($logo)) { echo '<div class="idt-Logo-image"><img src="'. check_url($logo) .'" alt="'. $site_title .'" id="logo" /></div>'; } 
    ?>
</div>

</div>
<div class="idt-nav">
    	    <div class="l"></div>
	    <div class="r"></div>
        	    <?php if (!empty($navigation)) { echo $navigation; }?>
	</div>
<?php if (!empty($banner1)) { echo $banner1; } ?>
<?php echo idt_placeholders_output($top1, $top2, $top3); ?>
<div class="idt-contentLayout">
<div class="<?php echo (!empty($right) || !empty($sidebar_right)) ? 'idt-content' : 'idt-content-wide'; ?>">
<?php if (!empty($banner2)) { echo $banner2; } ?>
<?php if ((!empty($user1)) && (!empty($user2))) : ?>
<table width="100%" cellpadding="0" cellspacing="0" border="0">
<tr valign="top"><td width="50%"><?php echo $user1; ?></td>
<td><?php echo $user2; ?></td></tr>
</table>
<?php else: ?>
<?php if (!empty($user1)) { echo '<div id="user1">'.$user1.'</div>'; }?>
<?php if (!empty($user2)) { echo '<div id="user2">'.$user2.'</div>'; }?>
<?php endif; ?>
<?php if (!empty($banner3)) { echo $banner3; } ?>
<div class="idt-Post">
    <div class="idt-Post-body">
<div class="idt-Post-inner">
<div class="idt-PostContent">
<?php if (!empty($breadcrumb)) { echo $breadcrumb; } ?>
<?php if (!empty($tabs)) { echo $tabs.'<div class="cleared"></div>'; }; ?>
<?php if (!empty($tabs2)) { echo $tabs2.'<div class="cleared"></div>'; } ?>
<?php if (!empty($mission)) { echo '<div id="mission">'.$mission.'</div>'; }; ?>
<?php if (!empty($help)) { echo $help; } ?>
<?php if (!empty($messages)) { echo $messages; } ?>
<?php echo idt_content_replace($content); ?>

</div>
<div class="cleared"></div>

</div>

    </div>
</div>
<?php if (!empty($banner4)) { echo $banner4; } ?>
<?php if (!empty($user3) && !empty($user4)) : ?>
<table width="100%" cellpadding="0" cellspacing="0" border="0">
<tr valign="top"><td width="50%"><?php echo $user3; ?></td>
<td><?php echo $user4; ?></td></tr>
</table>
<?php else: ?>
<?php if (!empty($user3)) { echo '<div id="user1">'.$user3.'</div>'; }?>
<?php if (!empty($user4)) { echo '<div id="user2">'.$user4.'</div>'; }?>
<?php endif; ?>
<?php if (!empty($banner5)) { echo $banner5; } ?>
</div>
<?php if (!empty($right)) echo '<div class="idt-sidebar1">' . $right . "</div>"; 
else if (!empty($sidebar_right)) echo '<div class="idt-sidebar1">' . $sidebar_right . "</div>";?>

</div>
<div class="cleared"></div>
<?php echo idt_placeholders_output($bottom1, $bottom2, $bottom3); ?>
<?php if (!empty($banner6)) { echo $banner6; } ?>
<div class="idt-Footer">
    <div class="idt-Footer-inner">
        <?php echo theme('feed_icon', url('rss.xml'), ''); ?>
        <div class="idt-Footer-text">
        <?php 
            if (!empty($footer_message) && (trim($footer_message) != '')) { 
                echo $footer_message;
            }
            else {
                echo '<p><a href="http://idthemes.sourceforge.net">FREE Drupal5 Themes</a> | <a href="http://idthemes.sourceforge.net">FREE Drupal6 Themes</a> | <a href="http://idthemes.sourceforge.net">FREE Drupal7 Themes</a> | <a href="http://idthemes.sourceforge.net">IDThemes Project</a><br />'.
                     'Copyright &copy; 2009&nbsp;'.$site_name.'.&nbsp;All Rights Reserved.</p>';
            }
        ?>
        <?php if (!empty($copyright)) { echo $copyright; } ?>
        </div>        
    </div>
    <div class="idt-Footer-background"></div>
</div>

    </div>
</div>
<div class="cleared"></div>
<p class="idt-page-footer"><a href="http://idthemes.sourceforge.net/">Drupal <?php { echo get_drupal_version(); } ?> Theme</a> by IDThemes</p>
</div>


<?php print $closure; ?>

</body>
</html>